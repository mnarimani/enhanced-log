using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Object = UnityEngine.Object;

namespace EnhancedLog
{
    public sealed class ELog
    {
        private readonly string type;

        public ELog(Type type)
        {
            this.type = type.Name;
        }

        public ELog(string type)
        {
            this.type = type;
        }

        public ELog(object type)
        {
            this.type = type.GetType().Name;
        }

        [DebuggerHidden]
        [DebuggerNonUserCode]
        [DebuggerStepThrough]
        [Conditional("DEBUG")]
        [Conditional("ENABLE_TRACE")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Trace(object message, Object context = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0)
        {
            LogHandler.HandleLog($"trce: ({type}) {message} \n[{memberName}:{lineNumber}]", context, LogLevel.Trace);
        }

        [DebuggerHidden]
        [DebuggerNonUserCode]
        [DebuggerStepThrough]
        [Conditional("DEBUG")]
        [Conditional("ENABLE_INFO")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Info(object message, Object context = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0)
        {
            LogHandler.HandleLog($"info: ({type}) {message} \n[{memberName}:{lineNumber}]", context, LogLevel.Info);
        }

        [DebuggerHidden]
        [DebuggerNonUserCode]
        [DebuggerStepThrough]
        [Conditional("DEBUG")]
        [Conditional("ENABLE_WARNING")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Warning(object message, Object context = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0)
        {
            LogHandler.HandleLog($"warn: ({type}) {message} \n[{memberName}:{lineNumber}]", context, LogLevel.Warning);
        }
        
        [DebuggerHidden]
        [DebuggerNonUserCode]
        [DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Error(object message, Object context = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0)
        {
            LogHandler.HandleLog($"fail: ({type}) {message} \n[{memberName}:{lineNumber}]", context, LogLevel.Error);
        }
        
        [DebuggerHidden]
        [DebuggerNonUserCode]
        [DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Fatal(object message, Object context = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0)
        {
            LogHandler.HandleLog($"crit: ({type}) {message} \n[{memberName}:{lineNumber}]", context, LogLevel.Fatal);
        }
    }
}